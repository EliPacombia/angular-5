import { Injectable } from '@angular/core';

@Injectable()
export class RopaService {
  //creamos la variable nombre_prenda con un valor definido
  public nombre_prenda = 'Pantalones Vaqueros';

  //creamos una variable del tipo array con sus valores
  public coleccion_ropa = ['Pantalones blancos', 'Camisa roja'];

  //metodo prueba
  prueba(nombre_prenda) {
    return nombre_prenda;
  }
  /***METODO GUARDAR POST ****/
  addRopa(nombre_prenda: string): Array<string> {
    //agregamos ropa con el input y el data binding
    this.coleccion_ropa.push(nombre_prenda);
    //recogemos lo que hay el getRopa
    return this.getRopa();
  }

  /**METODO ELIMINAR DELETE **/
  deleteRopa(index:number){
    //eliminamos  reconocieno el indice
    this.coleccion_ropa.splice(index,1);
    //retoramos con lo que queda
    return this.getRopa();
  }
    /*** METODO OBTENER  GET***/
  getRopa():Array<string> {
    //obtenemos lo que queda de la ropa
    return this.coleccion_ropa;
  }
}
