import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

//rutas
import {APP_ROUTING} from './app.routing';
//comopnentes
import { AppComponent } from './app.component';
import { EmpleadoComponent } from './components/empleado/empleado.component';
import { HomeComponent } from './components/home/home.component';
import { FrutasComponent } from './components/frutas/frutas.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ConversorPipe } from './pipes/conversor.pipe';



@NgModule({
  //directivas pipes y componentess
  declarations: [
    AppComponent,
    EmpleadoComponent,
    HomeComponent,
    FrutasComponent,
    ContactoComponent,
    NavbarComponent,
    ConversorPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
