import { RouterModule, Routes } from '@angular/router';

//importamos los componentes
import { EmpleadoComponent } from './components/empleado/empleado.component';
import { HomeComponent } from './components/home/home.component';
import { FrutasComponent } from './components/frutas/frutas.component';
import { ContactoComponent } from './components/contacto/contacto.component';

const APP_ROUTES: Routes = [


  { path: 'home', component: HomeComponent },
  { path: 'frutas', component: FrutasComponent },
  { path: 'empleado', component: EmpleadoComponent },
  { path: 'contacto', component: ContactoComponent },
  { path: 'contacto/:page', component: ContactoComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
