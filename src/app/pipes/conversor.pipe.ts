import { Pipe, PipeTransform } from '@angular/core';

//decorador para ponerle el nombre del pipe
@Pipe({
  name: 'conversor'
})
//implementamos la interfaz PipeTransform
export class ConversorPipe implements PipeTransform {

  //creamos el metodo transform ....
  transform(value, por){
  //convertidos de tipo string a number con el aprsei
  
  let value_one = parseInt(value);
  let value_two =parseInt(por);

  let result =" La multiplicacion es: " +value_one+ " x "+value_two+" = "+(value_one * value_two);
    return result;
  }

}
