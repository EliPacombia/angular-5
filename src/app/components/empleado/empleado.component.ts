//se importa del pakete node_modules
import { Component, OnInit } from '@angular/core';

//importamos los datos ya almacenador en un model, creado
import { Empleado } from './models-empleado';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html'
})
export class EmpleadoComponent implements OnInit {
  title = ' Componente Empleado';
  //declaramos que sea del tipo Empleado / el models que importamos
  public empleado: Empleado;

  //use del models/ en el caso de una array
  public trabajadores:Array<Empleado>;

  //creamos un variable boolean
  public trabajador_externo:boolean;

  public color:string; ////variable para una directiva

  public color_seleccionado:string; ///DATA-BINDIG
  constructor(){
    /********* EMPLEADO ****/
    //declaramos las variables o atributos que tiene nuestro models y colocamos una variable
    //una llamada de forma simple
    this.empleado = new Empleado('Leon',2343243,'Carpintero',25 ,true);
   /********Final EMPLEADO *****/

   /******* TRABAJADORES ARRAY ****/
    //llamada y asignacion de datos en un array
    this.trabajadores = [
    //ingresamo lo datos al array
      new Empleado('Jose',2111111,'Administador',45 ,false),
      new Empleado('Manalo',44444,'Pintor',27 ,true),
      new Empleado('Diaz',555555,'Cocinero',29 ,false),
      new Empleado('Paul',6688,'Panadero',22 ,true)];
      /******* Final TRABAJADORES ARRAY*********/

      //ingresamos el valor en forma global a la variable trabajador_externo
      this.trabajador_externo =true;

      this.color = 'blue'; //directiva de color

      this.color_seleccionado = '#ccc';//Data-BINDIG
  }
  ngOnInit() {
    //realizamos una muestra en el consola del navegador
    console.log(this.empleado);
    console.log(this.trabajadores);
  }
  //funcion de click(), cambio de valor a true y false
  cambiarExterno(valor){
    this.trabajador_externo= valor;
  }
  logColoreSeleccionado(){
    console.log(this.color_seleccionado);
  }
}
