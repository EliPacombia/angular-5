//aqui agregamos los datos de los modelos o los atributos
// del componente empleado
export class Empleado{

  constructor(
    public nombre:string,
    public dni:number,
    public cargo:string,
    public edad:number,
    public contratado:boolean
  ){}
}
