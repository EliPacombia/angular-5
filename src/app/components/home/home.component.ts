import { Component, OnInit } from '@angular/core';
//importamos nuestros servicio
import {RopaService} from '../../services/ropa.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',

  //agregamos la propiedad providers ****generar las dependencia del objeto**
  providers:[RopaService]
})
export class HomeComponent implements OnInit {
  public title=' Home Componente';
  public listado_ropa:Array<string>;

//creamos una variable prenda_a_guarda
  public prenda_a_guarda:string;
  constructor(
    //cramos una variable del tipo RopaService
    private _ropaService:RopaService
  ) { }

/*****MOSTRAMOS LA EL LISTAMOS DESDE EL servicio ****/
  ngOnInit() {
    this.listado_ropa = this._ropaService.getRopa();
    console.log(this.listado_ropa);
    //aqui most5ramos en la consola el metodo servicio prueba
    console.log(this._ropaService.prueba('Camiseta Nike'));
  }
  /*** FUNCION GUARDAR PRENDA ***/
  guardarPrenda(){
    this._ropaService.addRopa(this.prenda_a_guarda);
    this.prenda_a_guarda = null;
  }
  /****** FUNCION ELIMINAR PRENDA ****/
  eliminarPrenda(index:number){
    this._ropaService.deleteRopa(index);
    //alert(index);
  }
}
