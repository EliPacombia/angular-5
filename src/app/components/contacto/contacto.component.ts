import { Component, OnInit } from '@angular/core';
//importamos para realzar parametros en la url
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styles: []
})
export class ContactoComponent implements OnInit {
  public title = 'Pagina de contacto';
  public parametro; //creamos una varible para guardar el parametro de la url
  constructor(
    //propiedades
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    //mostramos el parametro con un forEach y funcion flecha
    this._route.params.forEach((params: Params) => {
    //el page se encurntra en la app.routing.ts
      this.parametro = params['page'];
    });
  }

  
  /********** ENRUTAR LOS PARAMETRO *******/
  redirigir(){
      this._router.navigate(['/contacto','eli Pacombia']);
  }
  redirigirDos(){
    this._router.navigate(['/home'])
  }
  /******* F ENRUTAR LOS PARAMETROS ********/
}
